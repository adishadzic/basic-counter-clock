import React from 'react';
import { useState, useEffect } from 'react';

function Counter() {
  let [counter, setCounter] = useState(0);

  useEffect(() => {
    setInterval(() => setCounter((counter) => counter + 1), 5000);
  }, []);

  return (
    <div>
      <h1>{counter}</h1>
    </div>
  );
}

export default Counter;
