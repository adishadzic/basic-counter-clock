import Clock from './components/Clock';
import Counter from './components/Counter';

function App() {
  return (
    <div className="App">
      <Counter />
      <Clock />
    </div>
  );
}

export default App;
